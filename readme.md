# HaveIBeenPwned for PHP

A small wrapper for HaveIBeenPwned for PHP using OOP

- By: Morten Harders (ExeQue)
- Website: http://usage.dk | https://gitlab.com/ExeQue
- Featured on: https://haveibeenpwned.com/API/Consumers
- Project: ~~http://usage.dk/projects/hibp~~ (Not available yet)

## Introduction

This wrapper is based on the API from http://haveibeenpwned.com
For full HaveIBeenPwned documentation visit [HaveIbeenPwned API Reference](https://haveibeenpwned.com/API/v2)

## Requirements
PHP-HIBP requires ext-curl and PHP7 or above. It does not run on older versions due to new syntax being used.

## Installation
### Composer
#### Command-line
```
composer require exeque/php-haveibeenpwned:dev-master
```
#### composer.json
```json
{
  "require": {
    "exeque/php-haveibeenpwned" : "dev-master"
  }
}
```

### Manual
Copy files from the src directory and drop in your classes folder, 
or into a directory named "ExeQue" if you're using a PSR-0 compliant autoloader

## Usage
PHP-HIBP requires instantiation before it can be used, 
which means multiple instances can be run at once, 
or wrapped in different wrappers using different setting sets.

PHP-HIBP is fully phpdoc described for IDE code completion and method descriptions

All breach methods return a model based on the [BreachModel](https://haveibeenpwned.com/API/v2#BreachModel) defined in [HaveIbeenPwned API Reference](https://haveibeenpwned.com/API/v2)

All cURL requests will retry every 3 seconds up to 10 times if the response is ```429 - Too Many Requests``` - See [Rate Limiting](https://haveibeenpwned.com/API/v2#RateLimiting)
### ```new ExeQue\HIBP(bool $ignore_ssl_verification = false)```
The constructor for PHP-HIBP can take an argument to ignore SSL verification errors (prominent on localhost development).
Simply pass ```true``` to the constructor. Whenever you request a resource it will notify if the SSL verification fails.
```php
$hibp = new ExeQue\HIBP();
```

### ```getBreachesForDomain(string $domain)```
Call this method to get information about breaches (if any) on a certain domain. It will return information about all breaches if the domain parameter is omitted.
```php
$adobe_breach = $hibp->getBreachesForDomain("adobe.com"); // Return information about breaches on adobe.com
$all_breaches = $hibp->getBreachesForDomain(); // Return information about all breaches in HaveIBeenPwned's database
```
### ```getBreachesForEmail(string $email, string $domain = null)```
Call this method to get information about breaches (if any) where the specified email is involved. You can optionally add a domain if you want to check a breached email against a certain domain.

E-mail address will be verified before sending the request.
```php
$my_breach    = $hibp->getBreachesForEmail("foo@bar.com"); // Return general breach information on foo@bar.com
$adobe_breach = $hibp->getBreachesForEmail("foo@bar.com", "adobe.com"); // Return breach information (if any) about foo@bar.com on adobe.com
```

### ```getDataClasses()```
Call this method to get all internal dataclasses from HaveIBeenPwned
```php
$hibp->getDataClasses();
```

## Inheritance
PHP-HIBP is made to be used as a stand-alone or inherited into an internal class in your system.

All methods can be overwritten, and all properties will be inherited.

## Support Methods / Constants
Several support methods and constants are publicly available from the class to manually cURL HaveIBeenPwned and validate the responses.

### Constants
%s in all constant values are a placeholder for any email entered alongside the constant (if needed) when calling ```$hibp->curl()```

#### Endpoints
```BREACHED_ACCOUNT_ENDPOINT => "breachedaccount/%s"```

```BREACHES_ENDPONT => "breaches/"```

```BREACH_ENDPOINT => "breach/%s"```

```DATA_CLASSES_ENDPOINT => "dataclasses/"```

#### Response Codes

```REQUEST_OK => 200```

```ERROR_400 => 400```

```ERROR_403 => 403```

```ERROR_429 => 429```

```ERROR_UNKNOWN => -1```

```ERROR_EMAIL => 1```

### Methods

Additional methods, besides the ones documented below, exists in the class - Please refer to the phpdoc

#### ```curl(string $endpoint, string $email = null)```
Manually cURL HaveIBeenPwned on a custom endpoint. 

Only supply $email if the endpoint has a placeholder for it!
```php
$hibp->curl(HIBP::BREACH_ENDPOINT, "foo@bar.com")
```

#### Get
Returns null or empty objects/arrays if run before ```$hibp->curl()``` or any breach method
##### ```getDataObject()```
Get response data mapped to an stdClass object

##### ```getDataArray()```
Get response data as an associative array

##### ```getResponseCode()```
Get HTTP response code

##### ```getDataLength()```
Get response length

#### Request Parameters / Headers
All set* methods must be run **before** before ```$hibp->curl()``` or any breach method.

##### ```setHeader(string $key, string $value)```
Set a single HTTP header

##### ```setHeaders(array $headers)```
Set a group of HTTP headers

##### ```getHeader(string $key)```
Get a single HTTP header

##### ```getHeaders()```
Get all set HTTP headers

##### ```buildHeaders()```
Build header array for cURL

##### ```setParameter(string $key, string $value)```
Set a single query parameter

##### ```setParameters(array $params)```
Set a group of query parameters

##### ```getParameter(string $key)```
Get a single query parameter

##### ```getHeaders()```
Get all set query parameters

##### ```buildQuery()```
Build the query string for the request