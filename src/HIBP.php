<?php

namespace ExeQue;

class HIBP {
    // region Constants
    // Base URL
    const BASE_URL = "https://haveibeenpwned.com/api/v2/";
    // Endponts
    const BREACHED_ACCOUNT_ENDPOINT = "breachedaccount/%s";
    const BREACHES_ENDPONT = "breaches/";
    const BREACH_ENDPOINT = "breach/%s";
    const DATA_CLASSES_ENDPOINT = "dataclasses/";
    // Response Message
    const REQUEST_OK = 200;
    const ERROR_400 = 400;
    const ERROR_403 = 403;
    const ERROR_429 = 429;
    const ERROR_UNKNOWN = -1;
    const ERROR_EMAIL = 1;
    // endregion

    // region Request Properties
    protected $headers = ["X-App-Requested-By" => "php-haveibeenpwned"];
    protected $parameters = [];
    protected $user_agent = "php-haveibeenpwned - https://gitlab.com/ExeQue/PHP-HIBP";
    protected $url = self::BASE_URL;
    protected $sleep_cycles = 0;
    // endregion

    // region Response Properties
    protected $response_data;
    protected $response_headers;
    // endregion

    // region Settings
    protected $ignore_ssl;
    // endregion

    /**
     * HIBP constructor.
     *
     * @param bool $ignore_ssl_verification Ignore certificate errors - Enable this if the cURL fails due to SSL certificate validation
     */
    public function __construct($ignore_ssl_verification = false) {

        $this->ignore_ssl = $ignore_ssl_verification;
    }

    // region Breaches Methods

    /**
     * Get breach info for a domain, or all domains
     *
     * @param string $domain Optional! Add domain to get breach info for a specific domain, leave as null to get all breaches
     *
     * @return \stdClass
     */
    public function getBreachesForDomain(string $domain = null) {
        if ($domain) $this->setParameter("domain", $domain);

        $this->curl(HIBP::BREACHES_ENDPONT);

        return $this->getDataObject();
    }

    /**
     * Get breach info for an e-mail on a domain, or on all domains
     *
     * @param string $email  Valid email
     * @param string $domain Optional! Add domain to get breach info for a specific domain, leave as null to get all breaches
     *
     * @return HIBP_Breach[]
     */
    public function getBreachesForEmail(string $email, string $domain = null) {
        if ($domain) $this->setParameter("domain", $domain);

        $this->curl(HIBP::BREACHED_ACCOUNT_ENDPOINT, $email);

        return HIBP_Breach::parseFromResponseData($this->getDataObject());
    }

    /**
     * Get all data classes
     *
     * @return \stdClass
     */
    public function getDataClasses() {
        $this->curl(HIBP::DATA_CLASSES_ENDPOINT);

        return $this->getDataObject();
    }
    // endregion

    // region Request Methods
    public function curl($endpoint, string $email = null) {
        if (self::validateEmail($email)) {
            $endpoint = $email ? sprintf($endpoint, urlencode($email)) : $endpoint;

            $url = self::BASE_URL . $endpoint . $this->buildQuery();
            $ch = curl_init($url);

            curl_setopt_array($ch, [
                CURLOPT_HTTPHEADER     => $this->buildHeaders(),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_USERAGENT      => $this->user_agent,
                CURLOPT_SSL_VERIFYHOST => $this->ignore_ssl ? 0 : 2,
                CURLOPT_SSL_VERIFYPEER => $this->ignore_ssl ? 0 : 2,
            ]);
            $this->response_data = curl_exec($ch);
            $this->response_headers = curl_getinfo($ch);

            if (curl_errno($ch) == 60) {
                die("cURL failed due to SSL certificate validation - Fix this in php.ini or set HIBP constructor parameter to true to ignore SSL validation (insecure) | " . get_class());
            }

            if ($this->getResponseCode() == 429 && $this->sleep_cycles < 10) {
                $this->sleep_cycles++;
                sleep(3);
                set_time_limit(10);

                return $this->curl($endpoint, $email);
            }

            return $this->getResponseCode();
        }

        return self::ERROR_EMAIL;
    }
    // endregion

    // region Data Methods
    /**
     * Get response data mapped to an stdClass object
     *
     * @return object
     */
    public function getDataObject() {
        return $this->response_data ? json_decode($this->response_data, false) : new \stdClass();
    }

    /**
     * Get response data as an associative array
     *
     * @return array
     */
    public function getDataArray() {
        return $this->response_data ? json_decode($this->response_data, true) : [];
    }

    /**
     * Get HTTP response code
     *
     * @return int
     */
    public function getResponseCode() {
        return (int)($this->response_headers["http_code"] ?? null);
    }

    /**
     * Get response length
     *
     * @return int
     */
    public function getDataLength() {
        return (int)($this->response_data && $this->response_data !== "[]" && $this->response_data ? strlen($this->response_data) : 0);
    }
    // endregion

    // region Parameter Methods
    /**
     * Set a single query parameter
     *
     * @param string $key
     * @param string $value
     *
     * @return $this
     */
    public function setParameter(string $key, string $value) {
        return $this->setSingle($key, $value, "parameters");
    }

    /**
     * Set a group of query parameters
     *
     * @param string[] $params
     *
     * @return $this
     */
    public function setParameters(array $params) {
        return $this->setMultiple($params, "parameters");
    }

    /**
     * Get a single query parameter
     *
     * @param $key
     *
     * @return null|string
     */
    public function getParameter($key) {
        return $this->getSingle($key, "parameters");
    }

    /**
     * Get all query parameters
     *
     * @return array|null
     */
    public function getParameters() {
        return $this->getMultiple("parameters");
    }

    /**
     * Build the query string for the request
     *
     * @return string
     */
    public function buildQuery() {
        return !empty($this->parameters) ? sprintf("/?%s", http_build_query($this->parameters)) : null;
    }
    // endregion

    // region Header Methods
    /**
     * Set a single HTTP header
     *
     * @param string $key   Header key
     * @param string $value Header value
     *
     * @return $this
     */
    public function setHeader(string $key, string $value) {
        return $this->setSingle($key, $value, "headers");
    }

    /**
     * Set a group of HTTP headers
     *
     * @param string[] $headers
     *
     * @return $this
     */
    public function setHeaders(array $headers) {
        return $this->setMultiple($headers, "headers");
    }

    /**
     * Get a single HTTP header
     *
     * @param string $key
     *
     * @return string|null
     */
    public function getHeader(string $key) {
        return $this->getSingle($key, "headers");
    }

    /**
     * Get all set HTTP headers
     *
     * @return array
     */
    public function getHeaders() {
        return $this->getMultiple("headers");
    }

    /**
     * Build header array for cURL
     *
     * @return array
     */
    public function buildHeaders() {
        $ret = [];
        foreach ($this->headers as $key => $header) {
            $ret[] = "$key: $header";
        }

        return $ret;
    }
    // endregion

    // region Validation Methods / Getter / Setter
    /**
     * Validates the entered email
     *
     * @param string $email
     *
     * @return string|false
     */
    public static function validateEmail($email) {
        return is_null($email) || filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Set a single property of the specified property array
     *
     * @param string $key
     * @param string $value
     * @param string $property
     *
     * @return $this
     */
    protected function setSingle(string $key, string $value, string $property) {
        if (isset($this->{$property}) && is_array($this->{$property})) {
            $this->{$property}[ $key ] = $value;
        }

        return $this;
    }

    /**
     * Set a group of properties for the specified property array
     *
     * @param array  $values
     * @param string $property
     *
     * @return $this
     */
    protected function setMultiple(array $values, string $property) {
        if (isset($this->{$property}) && is_array($this->{$property})) {
            foreach ($values as $key => $value) {
                if (!is_string($key) || !is_scalar($value)) {
                    unset($values[ $key ]);
                } else {
                    $this->{$property}[ $key ] = $value;
                }
            }
        }

        return $this;
    }

    /**
     * Get a single property from the specified property array
     *
     * @param string $key
     * @param string $property
     *
     * @return string|null
     */
    protected function getSingle(string $key, string $property) {
        return $this->{$property}[ $key ] ?? null;
    }

    /**
     * Get all properties from the specified property array
     *
     * @param string $property
     *
     * @return array|null
     */
    protected function getMultiple(string $property) {
        return $this->{$property} ?? null;
    }

    public static function parseResponseCode(int $code) {
        switch ($code) {
            case self::ERROR_400:
                return self::ERROR_400;
                break;
            case self::ERROR_403:
                return self::ERROR_403;
                break;
            case self::ERROR_429:
                return self::ERROR_429;
                break;
            case self::ERROR_EMAIL:
                return self::ERROR_EMAIL;
                break;
            case self::REQUEST_OK:
                return self::REQUEST_OK;
                break;
            default:
                return self::ERROR_UNKNOWN;
        }
    }
    // endregion
}