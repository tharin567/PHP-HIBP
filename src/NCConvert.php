<?php

namespace ExeQue;

class NCConvert {
    const PASCAL_CASE = 0;
    const LOWER_CASE = 1;
    const PROPER_CASE = 2;
    const CAMEL_CASE = 3;

    /**
     * Parse one naming convention to another
     *
     * @param string $name Original cased entry
     * @param int    $from Original casing style | User class defined constants
     * @param int    $to   Target casing style | User class defined constants
     *
     * @see \NCConvert::PASCAL_CASE
     * @see \NCConvert::LOWER_CASE
     * @see \NCConvert::PROPER_CASE
     * @see \NCConvert::CAMEL_CASE
     *
     * @return string Parsed string, or the original string if to/from pattern didn't match any parsers
     */
    public static function parse(string $name, int $from, int $to) {
        switch ($to) {
            case self::PASCAL_CASE:
                switch ($from) {
                    case self::LOWER_CASE:
                        return self::lowerCaseToPascalCase($name);
                        break;
                    case self::PROPER_CASE:
                        return self::properCaseToPascalCase($name);
                        break;
                    case self::CAMEL_CASE:
                        return self::camelCaseToPascalCase($name);
                }
                break;
            case self::LOWER_CASE:
                switch ($from) {
                    case self::PASCAL_CASE:
                        return self::pascalCaseToLowerCase($name);
                        break;
                    case self::PROPER_CASE:
                        return self::properCaseToLowerCase($name);
                        break;
                    case self::CAMEL_CASE:
                        return self::pascalCaseToLowerCase($name);
                }
                break;
            case self::PROPER_CASE:
                switch ($from) {
                    case self::PASCAL_CASE:
                        return self::pascalCaseToProperCase($name);
                        break;
                    case self::LOWER_CASE:
                        return self::lowerCaseToProperCase($name);
                        break;
                    case self::CAMEL_CASE:
                        return self::camelCaseToProperCase($name);
                }
                break;
            case self::CAMEL_CASE:
                switch ($from) {
                    case self::PASCAL_CASE:
                        return self::pascalCaseToCamelCase($name);
                        break;
                    case self::LOWER_CASE:
                        return self::lowerCaseToCamelCase($name);
                        break;
                    case self::PROPER_CASE:
                        return self::properCaseToCamelCase($name);
                }
                break;
        }

        return $name;
    }

    // region To PascalCase
    protected static function lowerCaseToPascalCase($key) {
        preg_replace_callback("/(^.{1}|[\_].{1}?)/", function ($elem) use (&$key) {
            if (!empty(array_filter($elem))) {
                $pos = strpos($key, $elem[0]);
                $char = str_replace("_", "", strtoupper($elem[0]));
                if ($pos === 0) {
                    $key = substr_replace($key, $char, $pos, strlen($elem[0]));
                } else {
                    $key = str_replace($elem[0], $char, $key);
                }
            }
        }, $key);

        return $key;
    }

    protected static function properCaseToPascalCase($key) {
        return str_replace("_", "", $key);
    }

    protected static function camelCaseToPascalCase($key) {
        preg_replace_callback("/(^.{1}|[A-Z]?)/", function ($elem) use (&$key) {
            if (!empty(array_filter($elem))) {
                $pos = strpos($key, $elem[0]);
                $char = strtoupper($elem[0]);
                if ($pos === 0) {
                    $key = substr_replace($key, $char, $pos, strlen($elem[0]));
                }
            }
        }, $key);

        return $key;
    }
    // endregion

    // region To LowerCase
    protected static function pascalCaseToLowerCase($key) {
        preg_replace_callback("/([A-Z]*?)/", function ($elem) use (&$key) {
            if (!empty(array_filter($elem))) {
                $pos = strpos($key, $elem[0]);
                if ($pos === 0) {
                    $key = substr_replace($key, strtolower($elem[0]), $pos, strlen($elem[0]));
                } else {
                    $key = substr_replace($key, "_" . strtolower($elem[0]), $pos, strlen($elem[0]));
                }
            }
        }, $key);

        return $key;
    }

    public static function properCaseToLowerCase($key) {
        return strtolower($key);
    }

    public static function camelCaseToLowerCase($key) {
        return self::pascalCaseToLowerCase($key);
    }
    // endregion

    // region To ProperCase
    public static function pascalCaseToProperCase($key) {
        preg_replace_callback("/([A-Z]?)/", function ($elem) use (&$key) {
            if (!empty(array_filter($elem))) {
                $pos = strpos($key, $elem[0]);
                $char = strtoupper($elem[0]);
                if ($pos !== 0) {
                    $key = substr_replace($key, "_$char", $pos, strlen($elem[0]));
                }
            }
        }, $key);

        return $key;
    }

    public static function camelCaseToProperCase($key) {
        return self::pascalCaseToProperCase(self::camelCaseToPascalCase($key));
    }

    public static function lowerCaseToProperCase($key) {
        return self::pascalCaseToProperCase(self::lowerCaseToPascalCase($key));
    }
    // endregion

    // region To CamelCase
    public static function pascalCaseToCamelCase($key) {
        $key[0] = strtolower($key[0]);

        return $key;
    }

    public static function lowerCaseToCamelCase($key) {
        return self::pascalCaseToCamelCase(self::lowerCaseToPascalCase($key));
    }

    public static function properCaseToCamelCase($key) {
        return self::pascalCaseToCamelCase(self::properCaseToPascalCase($key));
    }
    // endregion

    // region Test
    public static function test() {
        $strings = [
            "PASCAL_CASE" => "IsThisWorking",
            "LOWER_CASE"  => "is_this_working",
            "PROPER_CASE" => "Is_This_Working",
            "CAMEL_CASE"  => "isThisWorking",
        ];

        $rf = new \ReflectionClass(get_class());
        $consts = $rf->getConstants();

        $res = ["<tr><th>From</th><th>To</th><th>Origin</th><th>Result</th></tr>"];
        foreach ($strings as $key => $string) {
            foreach ($consts as $const => $val) {
                if ($const !== $key) {
                    $td = "<td style='border: 1px solid black; padding: 3px;'>%s</td>";
                    $res[] = sprintf("<tr>%s%s%s%s</tr>", sprintf($td, $key), sprintf($td, $const), sprintf($td, $string),
                                     sprintf($td, self::parse($string, $rf->getConstant($key), $rf->getConstant($const))));
                }
            }
        }

        die(sprintf("<div style='border: 1px solid black; text-align:center; display: inline-block;'><h1>NCConvert Test</h1><table style='border: 1px solid black; border-collapse: collapse;'>%s</table></div>",
                    implode("\n", $res)));
    }
    // endregion
}